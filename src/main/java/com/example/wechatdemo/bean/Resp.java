package com.example.wechatdemo.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zero on 2020/9/28.
 * desc:
 */
public class Resp {
   private String msg;
   private Integer code;
   private List<Gps> data = new ArrayList<>();

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Gps> getData() {
        return data;
    }

    public void setData(List<Gps> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Resp{" +
                "msg='" + msg + '\'' +
                ", code=" + code +
                ", data=" + data +
                '}';
    }
}
