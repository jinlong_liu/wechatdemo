package com.example.wechatdemo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

@TableName("sys_appuser")
public class SysAppuser implements Serializable {
    @TableId(type= IdType.AUTO,value = "appuser_id")
    private Integer appuserId;
    private String userName;
    private Date createDate;
    private Integer electricBikes;
    private String familyAddress;
    private Integer carCount;
    private Integer familyMumberCount;
    private String openid;

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getElectricBikes() {
        return electricBikes;
    }

    public void setElectricBikes(Integer electricBikes) {
        this.electricBikes = electricBikes;
    }

    public String getFamilyAddress() {
        return familyAddress;
    }

    public void setFamilyAddress(String familyAddress) {
        this.familyAddress = familyAddress;
    }

    public Integer getCarCount() {
        return carCount;
    }

    public void setCarCount(Integer carCount) {
        this.carCount = carCount;
    }

    public Integer getFamilyMumberCount() {
        return familyMumberCount;
    }

    public void setFamilyMumberCount(Integer familyMumberCount) {
        this.familyMumberCount = familyMumberCount;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Override
    public String toString() {
        return "SysAppuser{" +
                "appuserId=" + appuserId +
                ", userName='" + userName + '\'' +
                ", createDate=" + createDate +
                ", electricBikes=" + electricBikes +
                ", familyAddress='" + familyAddress + '\'' +
                ", carCount=" + carCount +
                ", familyMumberCount=" + familyMumberCount +
                ", openid=" + openid +
                '}';
    }
}
