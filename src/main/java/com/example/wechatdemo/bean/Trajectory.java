package com.example.wechatdemo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

public class Trajectory {
    @TableId(type= IdType.AUTO,value = "id")
    private Integer id;
    private Integer appuserId;
    private Date startDate;
    private Date endDate;
    private String trajectoryGps;
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }


    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTrajectoryGps() {
        return trajectoryGps;
    }

    public void setTrajectoryGps(String trajectoryGps) {
        this.trajectoryGps = trajectoryGps;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Trajectory{" +
                "id=" + id +
                ", appuserId=" + appuserId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", trajectoryGps='" + trajectoryGps + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
