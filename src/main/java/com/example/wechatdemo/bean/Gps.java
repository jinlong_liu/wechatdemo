package com.example.wechatdemo.bean;

import com.example.wechatdemo.utils.DateUtils;

/**
 * Created by zero on 2020/9/28.
 * desc:
 */
public class Gps {
    private String address;
    private String city;
    private Double lng;
    private Double lat;
    private Long timestamp;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Gps{" +
                "address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", lng=" + lng +
                ", lat=" + lat +
                ", timestamp=" + DateUtils.stringToDate(timestamp) +
                '}'+"\n";
    }
}
