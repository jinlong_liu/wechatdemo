package com.example.wechatdemo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("app_family_information")
public class AppFamilyInformation {
    @TableId(type= IdType.AUTO,value = "id")
    private Integer familyId;
    private Integer appuserId;
    private Integer sex;
    private Integer age;
    private Integer isPermanentAddress;
    private Integer income;
    private Integer profession;
    private String companyAddress;

    public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getIsPermanentAddress() {
        return isPermanentAddress;
    }

    public void setIsPermanentAddress(Integer isPermanentAddress) {
        this.isPermanentAddress = isPermanentAddress;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getProfession() {
        return profession;
    }

    public void setProfession(Integer profession) {
        this.profession = profession;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    @Override
    public String toString() {
        return "AppFamilyInformation{" +
                "familyId=" + familyId +
                ", appuserId=" + appuserId +
                ", sex=" + sex +
                ", age=" + age +
                ", isPermanentAddress=" + isPermanentAddress +
                ", income=" + income +
                ", profession=" + profession +
                ", companyAddress='" + companyAddress + '\'' +
                '}';
    }
}
