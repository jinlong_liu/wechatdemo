package com.example.wechatdemo.bean;

/**
 * Created by zero on 2020/9/28.
 * desc:
 */
public class Body {
    String id;
    Long time;
    String nonce;
    String appKey ;
    String imei;
    Long beginTime;
    Long endTime;

    public Body() {
    }

    public Body(String id, Long time, String nonce, String appKey, String imei) {
        this.id = id;
        this.time = time;
        this.nonce = nonce;
        this.appKey = appKey;
        this.imei = imei;
    }

    public Body(String id, Long time, String nonce, String appKey, String imei, Long beginTime, Long endTime) {
        this.id = id;
        this.time = time;
        this.nonce = nonce;
        this.appKey = appKey;
        this.imei = imei;
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    public Long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Long beginTime) {
        this.beginTime = beginTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }


}
