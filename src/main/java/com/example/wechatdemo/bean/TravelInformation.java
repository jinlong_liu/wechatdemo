package com.example.wechatdemo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("travel_information")
public class TravelInformation {
    @TableId(type= IdType.AUTO,value = "id")
    private Integer id;
    private Integer appuserId;
    private Date startDate;
    private Date endDate;
    private String startAddress;
    private String endAddress;
    private String startGps;
    private String endGps;
    private Integer tripMode;
    private Integer tripObjective;
    private Integer status;
    private Integer ranking;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }


    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getStartGps() {
        return startGps;
    }

    public void setStartGps(String startGps) {
        this.startGps = startGps;
    }

    public String getEndGps() {
        return endGps;
    }

    public void setEndGps(String endGps) {
        this.endGps = endGps;
    }

    public Integer getTripMode() {
        return tripMode;
    }

    public void setTripMode(Integer tripMode) {
        this.tripMode = tripMode;
    }

    public Integer getTripObjective() {
        return tripObjective;
    }

    public void setTripObjective(Integer tripObjective) {
        this.tripObjective = tripObjective;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return "TravelInformation{" +
                "id=" + id +
                ", appuserId=" + appuserId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", startAddress='" + startAddress + '\'' +
                ", endAddress='" + endAddress + '\'' +
                ", startGps='" + startGps + '\'' +
                ", endGps='" + endGps + '\'' +
                ", tripMode=" + tripMode +
                ", tripObjective=" + tripObjective +
                ", status=" + status +
                ", ranking=" + ranking +
                '}';
    }
}
