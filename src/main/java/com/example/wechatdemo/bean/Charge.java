package com.example.wechatdemo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("charge")
public class Charge {
    @TableId(type= IdType.AUTO,value = "id")
    private Integer id;
    private Integer appuserId;
    private Integer isChargeStatus;
    private Integer extended2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppuserId() {
        return appuserId;
    }

    public void setAppuserId(Integer appuserId) {
        this.appuserId = appuserId;
    }

    public Integer getIsChargeStatus() {
        return isChargeStatus;
    }

    public void setIsChargeStatus(Integer isChargeStatus) {
        this.isChargeStatus = isChargeStatus;
    }

    public Integer getExtended2() {
        return extended2;
    }

    public void setExtended2(Integer extended2) {
        this.extended2 = extended2;
    }
}
