package com.example.wechatdemo.utils;

import com.alibaba.fastjson.JSON;
import com.example.wechatdemo.bean.Body;
import com.example.wechatdemo.bean.Gps;
import com.example.wechatdemo.bean.Resp;
import com.example.wechatdemo.bean.Trajectory;
import org.apache.commons.collections.MapUtils;
import org.apache.http.*;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.config.ScheduledTask;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description:
 * @author: aaron
 * @create: 2019-12-30 17:13
 **/
public class HttpUtil{
    private static final String HTTP = "http";
    private static final String HTTPS = "https";
    private static SSLConnectionSocketFactory sslsf = null;
    private static PoolingHttpClientConnectionManager cm = null;
    private static SSLContextBuilder builder = null;
    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    static {
        try {
            builder = new SSLContextBuilder();
            // 全部信任 不做身份鉴定
            builder.loadTrustMaterial(null, new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    return true;
                }
            });
            sslsf = new SSLConnectionSocketFactory(builder.build(), new String[]{"SSLv2Hello", "SSLv3", "TLSv1", "TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register(HTTP, new PlainConnectionSocketFactory())
                    .register(HTTPS, sslsf)
                    .build();
            cm = new PoolingHttpClientConnectionManager(registry);
            cm.setMaxTotal(200);//max connection
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CloseableHttpClient getHttpClient() throws Exception {
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .setConnectionManager(cm)
                .setConnectionManagerShared(true).build();
        return httpClient;
    }

    public static String get(String url, Map<String, String> param) {
        //单位毫秒
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(10000)
                .setConnectTimeout(10000)
                .setSocketTimeout(10000).build();

        CloseableHttpClient httpClient = null;
        HttpGet httpGet = null;
        try {
            URIBuilder builder = new URIBuilder(url);
            // 设置请求参数
            if (MapUtils.isNotEmpty(param)) {
                List<NameValuePair> formparams = new ArrayList<NameValuePair>();
                for (Map.Entry<String, String> entry : param.entrySet()) {
                    formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                builder.setParameters(formparams);
            }
            httpGet = new HttpGet(builder.build());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        httpGet.setConfig(requestConfig);

        CloseableHttpResponse response = null;
        try {
            httpClient = getHttpClient();
            response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                return null;
            } else {
                return EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (Exception e) {
            logger.error("httpGet Exception handle-- > " + e);
        } finally {
            if (response != null){
                try {
                    response.close();//关闭response
                } catch (IOException e) {
                    logger.error("httpGet IOException handle-- > " + e);
                }
            }
            if(httpClient != null){
                try {
                    httpClient.close();
                } catch (IOException e) {
                    logger.error("httpGet IOException handle-- > " + e);
                }
            }
        }
        return null;
    }

    /**
     * httpClient post请求
     * @param url 请求url
     * @param header 头部信息
     * @param param 请求参数 form提交适用
     * @param entity 请求实体 json/xml提交适用
     * @return 可能为空 需要处理
     * @throws Exception
     */
    public static String post(String url, Map<String, String> header, Map<String, String> param, HttpEntity entity) throws Exception {
        String result = "";
        CloseableHttpClient httpClient = null;
        httpClient = getHttpClient();
        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();
        httpPost.setConfig(requestConfig);
        try {
            // 设置头信息
            if (MapUtils.isNotEmpty(header)) {
                for (Map.Entry<String, String> entry : header.entrySet()) {
                    httpPost.addHeader(entry.getKey(), entry.getValue());
                }
            }
            // 设置请求参数
            if (MapUtils.isNotEmpty(param)) {
                List<NameValuePair> formparams = new ArrayList<NameValuePair>();
                for (Map.Entry<String, String> entry : param.entrySet()) {
                    //给参数赋值
                    formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
                httpPost.setEntity(urlEncodedFormEntity);
            }
            // 设置实体 优先级高
            if (entity != null) {
                httpPost.setEntity(entity);
            }
            HttpResponse httpResponse = httpClient.execute(httpPost);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity resEntity = httpResponse.getEntity();
                result = EntityUtils.toString(resEntity);
            } else {
                readHttpResponse(httpResponse);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("httpPost IOException handle-- > " + e);
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }
        return result;
    }

    public static String readHttpResponse(HttpResponse httpResponse)
            throws ParseException, IOException {
        StringBuilder builder = new StringBuilder();
        // 获取响应消息实体
        HttpEntity entity = httpResponse.getEntity();
        // 响应状态
        builder.append("status:" + httpResponse.getStatusLine());
        builder.append("headers:");
        HeaderIterator iterator = httpResponse.headerIterator();
        while (iterator.hasNext()) {
            builder.append("\t" + iterator.next());
        }
        // 判断响应实体是否为空
        if (entity != null) {
            String responseString = EntityUtils.toString(entity);
            builder.append("response length:" + responseString.length());
            builder.append("response content:" + responseString.replace("\r\n", ""));
        }
        return builder.toString();
    }

    public static void main(String[] args) throws Exception {
        Map<String ,Object> signMap = new HashMap<String ,Object>();
        String id= UUID.randomUUID().toString();
        Long time = System.currentTimeMillis();
        String nonce = "12345";
        String appKey = "21068123";
        String imei="863659040029588";
        //String imei="863659040064221";

        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        //String format = simpleDateFormat.format(new Date());
        //Long startTime = simpleDateFormat.parse(format).getTime()-86400000L;
        //Long endTime=time;
        //Long startTime = DateUtils.FromString2Date("2020-10-13 00:00:01").getTime();
        Long startTime = DateUtils.FromString2Date("2020-10-13 12:00:00").getTime();

        //Long startTime = simpleDateFormat.parse(format).getTime();
        //double timePrint1 = 1602561673000d;
        //double timePrint2 = 1602561673000d;
        //String result2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timePrint1);
        //System.out.println("13位数的时间戳（毫秒）--->Date1:" + result2);

//        String result3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timePrint2);
//        System.out.println("13位数的时间戳（毫秒）--->Date2:" + result3);


        Long endTime = DateUtils.FromString2Date("2020-10-13 12:05:00").getTime();
        signMap.put("id",id);
        signMap.put("time",time);
        signMap.put("nonce",nonce);
        signMap.put("appKey",appKey);
        signMap.put("imei",imei);
        signMap.put("beginTime",startTime);
        signMap.put("endTime",endTime);



        String appSecret = "guob78xg2vdfvch4jdjhgajcklg5eilg";
        String sign = SignUtil.sign(signMap,appSecret);
        System.out.println("sign = " + sign);
        Map<String ,String> headerMap = new HashMap<String ,String>();
        headerMap.put("Content-Type", "application/json");
        headerMap.put("sign", sign);
        System.out.println("headerMap = " + headerMap);

        String body = JSON.toJSONString(new Body(id,time,nonce,appKey,imei,startTime,endTime));
        System.out.println("body = " + body);

        HttpEntity entity = new StringEntity(body, "UTF-8");
        String result = HttpUtil.post("http://api.dmp.toptolink.com/dmp/device/v1.0/getTrack", headerMap, null, entity);
        //System.out.println("结果:"+result);
        Resp resp = JSON.parseObject(result, Resp.class);
        System.out.println(JSON.parseObject(result, Resp.class).toString());
        System.out.println("size:"+resp.getData().size());
    }
    public synchronized static List<Trajectory> getGpsByIMEI(String IMEI,Integer userId) throws Exception{
        Map<String ,Object> signMap = new HashMap<String ,Object>();
        String id= UUID.randomUUID().toString();
        Long time = System.currentTimeMillis();
        String nonce = "12345";
        String appKey = "21068123";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String format = simpleDateFormat.format(new Date());
        Long startTime = simpleDateFormat.parse(format).getTime();
        //System.out.println(new Date(startTime));

        signMap.put("id",id);
        signMap.put("time",time);
        signMap.put("nonce",nonce);
        signMap.put("appKey",appKey);
        signMap.put("imei", IMEI);
        signMap.put("beginTime",startTime);
        signMap.put("endTime",time);



        String appSecret = "guob78xg2vdfvch4jdjhgajcklg5eilg";
        String sign = SignUtil.sign(signMap,appSecret);
        System.out.println("sign = " + sign);
        Map<String ,String> headerMap = new HashMap<String ,String>();
        headerMap.put("Content-Type", "application/json");
        headerMap.put("sign", sign);
        System.out.println("headerMap = " + headerMap);

        String body = JSON.toJSONString(new Body(id,time,nonce,appKey, IMEI,startTime, time));
        System.out.println("body = " + body);

        HttpEntity entity = new StringEntity(body, "UTF-8");
        String result = HttpUtil.post("http://api.dmp.toptolink.com/dmp/device/v1.0/getTrack", headerMap, null, entity);
        System.out.println(JSON.parseObject(result, Resp.class).toString());
        logger.info("获取到的信息："+JSON.parseObject(result, Resp.class).toString());
        List<Gps> data = JSON.parseObject(result, Resp.class).getData();
        if(data==null||data.size()==0){
            return null;
        }
        List<Trajectory> trajectories = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Gps datum : data) {
            Trajectory trajectory = new Trajectory();
            trajectory.setAppuserId(userId);
            trajectory.setStartDate(new Date(datum.getTimestamp()));
            trajectory.setEndDate(new Date(datum.getTimestamp()));
            trajectory.setAddress(datum.getAddress());
            String Gps = datum.getLng()+","+datum.getLat();
            trajectory.setTrajectoryGps(Gps);
            trajectories.add(trajectory);
        }
        return trajectories;

    }
}
