package com.example.wechatdemo.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.*;

public class SignUtil {
    /**
     * 使用<code>secret</code>对paramValues按以下算法进行签名： <br/>
     * uppercase(hex(sha1(secretkey1value1key2value2...secret))
     *
     * @param paramValues 参数列表
     * @param secret
     * @return
     */
    public static String sign(Map<String, Object> paramValues, String secret) {
        return sign(paramValues, null, secret);
    }

    /**
     * 对paramValues进行签名，其中ignoreParamNames这些参数不参与签名
     *
     * @param paramValues
     * @param ignoreParamNames
     * @param secret
     * @return
     */
    public static String sign(Map<String, Object> paramValues, List<String> ignoreParamNames, String secret) {
        try {
            StringBuilder sb = new StringBuilder();
            List<String> paramNames = new ArrayList<String>(paramValues.size());
            paramNames.addAll(paramValues.keySet());
            if (ignoreParamNames != null && ignoreParamNames.size() > 0) {
                for (String ignoreParamName : ignoreParamNames) {
                    paramNames.remove(ignoreParamName);
                }
            }
            Collections.sort(paramNames);
            for (int i = 0; i < paramNames.size(); i++) {
                String key = paramNames.get(i);
                String value = paramValues.get(key).toString();
                sb.append(key + "=" + value + "&");
            }
            sb.append("appSecret="+secret);
            System.out.println(sb.toString());
            byte[] sha1Digest = getSHA1Digest(sb.toString());
            return byte2hex(sha1Digest);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] getSHA1Digest(String data) throws IOException {
        byte[] bytes = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            bytes = md.digest(data.getBytes("UTF-8"));
        } catch (GeneralSecurityException gse) {
            throw new IOException(gse);
        }
        return bytes;
    }

    /**
     * 二进制转十六进制字符串
     *
     * @param bytes
     * @return
     */
    private static String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append("0");
            }
            sign.append(hex.toUpperCase());
        }
        return sign.toString();
    }

    /**
     * 测试用：组装相关参数
     *
     * @param params
     * @return
     */
    private static String joinParams(Map<String, String> params) {
        String res = "?";
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            res += key + "=" + value + "&";
        }
        res = StringUtils.substring(res, 0, res.length() - 1);
        return res;
    }

    public static void main(String args[]) {
		String appKey = "1000006";
		String appSecret = "dzwh1wcxvlcu8grwgkhdff5u7x57m608";
		
        Map<String, Object> paramValues = new HashMap<>();
        paramValues.put("id", "1234567890123456");
        paramValues.put("time", Long.parseLong("1586524794793"));
        paramValues.put("nonce", "12345");
        paramValues.put("appKey", appKey);

        JSONObject body = (JSONObject) JSONObject.toJSON(paramValues);
        System.out.println("bodyStr:"+body.toJSONString());

        String signStr = sign(body, appSecret);
        System.out.println("signStr:"+signStr);

        Map<String ,String> headerMap = new HashMap<String ,String>();
        headerMap.put("Content-Type", "application/json");
        headerMap.put("sign", signStr);

        HttpEntity entity = new StringEntity(body.toJSONString(), "UTF-8");
        try {
            String result = HttpUtil.post("http://api.dmp.toptolink.com/dmp/device/v1.0/getInfo", headerMap, null, entity);
            System.out.println("result:"+result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
