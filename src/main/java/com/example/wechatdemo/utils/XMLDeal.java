package com.example.wechatdemo.utils;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLDeal {

    public static Map<String,String> parseRequst(HttpServletRequest request){
        Map<String,String> map =new HashMap<>();
        try{
            SAXReader reader = new SAXReader();
            Document read = reader.read(request.getInputStream());
            Element root = read.getRootElement();
            List<Element> elementList=root.elements();
            for(Element e:elementList){
                map.put(e.getName(),e.getStringValue());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return map;
    }
}
