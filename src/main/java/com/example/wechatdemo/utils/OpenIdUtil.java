package com.example.wechatdemo.utils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OpenIdUtil {
    /**
     * 获取关注本公众号的所有用户的openid
     * @return
     */
    public static List<String> getAllOpenId(){
        String url="https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN";
        url=url.replace("ACCESS_TOKEN",TokenUtil.getAccessToken().getAccessToken());
        JSONObject result = JSONObject.fromObject(TokenUtil.get(url));
        Object data = result.get("data");
        JSONObject jsonObject = JSONObject.fromObject(data);
        List<String> openids = new ArrayList<>();
        JSONArray openid = jsonObject.getJSONArray("openid");
        for (Object o : openid) {
            openids.add((String)o);
        }
        return openids;
    }
}
