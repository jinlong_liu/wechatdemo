package com.example.wechatdemo.utils;

import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PushUtil {

    protected static final Logger logger = LoggerFactory.getLogger(PushUtil.class);
    //模板消息的ID
    private static final String TEMPLATE = "ZGbnvZHudIPN_pLqECbfn-VXk-RbtXnSP4hDeyz2EFs";
    private static final String APPID = "wx1c6fb297bf4bf502";
    /**
     * 下发微信模板消息
     * @param openids
     * @return
     */
    public static void pushMessage(List<String> openids){
        String url=" https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        url=url.replace("ACCESS_TOKEN",TokenUtil.getAccessToken().getAccessToken());
        for (String openid : openids) {
            JSONObject template = new JSONObject();
            Map<Object,Object> map =new HashMap<>();
            Map<Object,Object> data =new HashMap<>();
            Map<Object,Object> first =new HashMap<>();
            Map<Object,Object> keyword1 =new HashMap<>();
            Map<Object,Object> keyword2 =new HashMap<>();
            Map<Object,Object> keyword3 =new HashMap<>();
            Map<Object,Object> remark =new HashMap<>();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            first.put("value","您好");
            first.put("color","#173177");
            //keyword1.put("value",returnAppuser.getUserName());
            keyword1.put("value","志愿者");
            keyword1.put("color","#173177");
            keyword2.put("value","您有出行信息未完善，请及时完善出行信息");
            keyword2.put("color","#173177");
            keyword3.put("value",simpleDateFormat.format(new Date()));
            keyword3.put("color","#173177");
            remark.put("value","感谢您的使用！");
            remark.put("color","#173177");
            data.put("first",first);
            data.put("keyword1",keyword1);
            data.put("keyword2",keyword2);
            data.put("keyword3",keyword3);
            data.put("remark",remark);
            map.put("touser",openid);
            map.put("template_id",TEMPLATE);
            map.put("appid",APPID);
            map.put("data",data);
            template.putAll(map);
            logger.info("推送消息的json："+map.toString());
            TokenUtil.post(url,template.toString());
        }
    }
}
