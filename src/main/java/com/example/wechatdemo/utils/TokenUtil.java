package com.example.wechatdemo.utils;


import com.example.wechatdemo.bean.AccessToken;
import net.sf.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class TokenUtil {
    private static String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx1c6fb297bf4bf502&secret=3d8bcf78c3a68aec0e255094f172fd33";
    public static final String APPID = "wx1c6fb297bf4bf502";
    private static final String APPSECRET = "3d8bcf78c3a68aec0e255094f172fd33";
    private static AccessToken accessToken1;

    /**
     * 用来发送post请求
     * @param url
     * @param data
     * @return
     */
    public static String post(String url,String data){
        try {
            URL urlConnection = new URL(url);
            URLConnection connection = urlConnection.openConnection();
            connection.setDoOutput(true);
            OutputStream os =connection.getOutputStream();
            os.write(data.getBytes());
            os.close();
            InputStream inputStream = connection.getInputStream();
            byte[] bytes = new byte[1024];
            int len;
            StringBuilder stringBuilder = new StringBuilder();
            while ((len=inputStream.read(bytes))!=-1){
                stringBuilder.append(new String(bytes,0,len));
            }
            System.out.println(stringBuilder.toString());
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 用来发送get请求
     * @param url
     * @return
     */
    public static String get(String url){
        try {
            URL urlConnection = new URL(url);
            URLConnection urlConnection1 = urlConnection.openConnection();
            InputStream inputStream = urlConnection1.getInputStream();
            byte[] bytes = new byte[1024];
            int len;
            StringBuilder stringBuilder = new StringBuilder();
            while ((len=inputStream.read(bytes))!=-1){
                stringBuilder.append(new String(bytes,0,len));
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private static void getToken(String string){
        JSONObject jsonObject = JSONObject.fromObject(string);
        String token = jsonObject.getString("access_token");
        String expiresIn = jsonObject.getString("expires_in");
        accessToken1= new AccessToken(token,expiresIn);
    }

    /**
     * 用来获取token的方法，当时写的很乱，但是不想改了，，，，
     * @return
     */
    public static AccessToken getAccessToken(){
        if(accessToken1==null||accessToken1.isExpired()){
            System.out.println("getToken");
            getToken(get(GET_TOKEN_URL));
            System.out.println("getTokenSuccess");
        }
        return accessToken1;
    }
}
