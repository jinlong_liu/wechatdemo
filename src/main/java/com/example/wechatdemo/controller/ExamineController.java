package com.example.wechatdemo.controller;

import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.bean.TravelInformation;
import com.example.wechatdemo.service.ExamineService;
import com.example.wechatdemo.service.RegisterService;
import com.mysql.cj.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
public class ExamineController {

    @Autowired
    private RegisterService registerService;
    @Autowired
    private ExamineService examineService;

    @GetMapping("/improve")
    public String improve(String nickname, String openid, String headimgurl, Model model, HttpSession session){
        System.out.println("nickname = " + nickname + ", openid = " + openid + ", headimgurl = " + headimgurl);
        SysAppuser register = registerService.isRegister(openid);
        if(register==null){
            model.addAttribute("nickname","您还未注册！");
            model.addAttribute("openid",openid);
            model.addAttribute("headimgurl",headimgurl);
            return "improve";
        }
        List<TravelInformation> allTravelInformation = examineService.getAllTravelInformation(register);
        session.setAttribute("user",register);
        model.addAttribute("travels",allTravelInformation);
        model.addAttribute("nickname",nickname);
        model.addAttribute("openid",openid);
        model.addAttribute("headimgurl",headimgurl);
        return "improve";
    }
    @GetMapping("/travel")
    public String travel(String travelId,Model model){
        TravelInformation travel = examineService.getTravelById(travelId);
        Date startDate = travel.getStartDate();
        Date endDate = travel.getEndDate();
        if(startDate.after(endDate)){
            travel.setStartDate(endDate);
            travel.setEndDate(startDate);
        }

        if(!travel.getStartAddress().substring(travel.getStartAddress().length()-2).equals("附近")){
            travel.setStartAddress(travel.getStartAddress()+"附近");
            travel.setEndAddress(travel.getEndAddress()+"附近");
        }
        System.out.println("travel = " + travel);
        model.addAttribute("travel",travel);
        return "travel";
    }
    @PostMapping("perfectTravel")
    @ResponseBody
    public int prifectTravel(Integer travelId, String startDate,
                             String endDate,String startAddress,String endAddress,Integer tripMode,Integer tripObject,HttpSession session){
        System.out.println("travelId = " + travelId + ", startDate = " + startDate + ", endDate = " + endDate + ", startAddress = " + startAddress + ", endAddress = " + endAddress + ", tripMode = " + tripMode + ", tripObject = " + tripObject);
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date start = format.parse(startDate);
            Date end = format.parse(endDate);
            System.out.println(start);
            System.out.println(end);
            return examineService.updateTravel(travelId,start,end,startAddress,endAddress,tripMode,tripObject,session);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }
//    @GetMapping("/getTravel")
//    @ResponseBody
//    public List<TravelInformation> getTravel(HttpSession session){
//        System.out.println("ExamineController.getTravel");
//        SysAppuser user = (SysAppuser) session.getAttribute("user");
//        return examineService.getAllTravelInformation(user);
//    }
}
