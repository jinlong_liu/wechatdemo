package com.example.wechatdemo.controller;

import com.example.wechatdemo.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RegisterController {
    @Autowired
    private RegisterService registerService;

    @PostMapping("/register")
    @ResponseBody
    public int register(String openid,String username,String address,String macAddress,Integer car,Integer bike
    ,Integer sex,Integer age,Integer profession,Integer isLocal,Integer income,String phone,String companyAddress){
        macAddress = macAddress.toUpperCase();
        System.out.println("=====================================================");
        System.out.println("openid = " + openid);
        System.out.println("username = " + username);
        System.out.println("address = " + address);
        System.out.println("macAddress = " + macAddress);
        System.out.println("car = " + car);
        System.out.println("bike = " + bike);
        System.out.println("sex = " + sex);
        System.out.println("age = " + age);
        System.out.println("profession = " + profession);
        System.out.println("isLocal = " + isLocal);
        System.out.println("income = " + income);
        System.out.println("phone = " + phone);
        System.out.println("判断蛋疼的表情是啥:"+username);
        System.out.println("判断蛋疼的表情是不是空的:"+username.equals(""));
        return registerService.register(openid, username, address, macAddress, car, bike, sex, age, profession, isLocal, income, phone,companyAddress);
    }
}
