package com.example.wechatdemo.controller;


import com.example.wechatdemo.service.RegisterService;
import com.example.wechatdemo.service.WxService;
import com.example.wechatdemo.utils.EncodeUtil;
import com.example.wechatdemo.utils.OpenIdUtil;
import com.example.wechatdemo.utils.TokenUtil;
import com.example.wechatdemo.bean.*;
import com.example.wechatdemo.utils.XMLDeal;
import com.mysql.cj.Session;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.ScheduledTask;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class WechatController {

    protected static final Logger logger = LoggerFactory.getLogger(WechatController.class);
    private static final String URL = "http://www.citydatas.com";

    @Autowired
    private WxService wxService;
    @Autowired
    private RegisterService registerService;

    /**
     * 用于绑定公众号，微信公众号发送一个请求到该接口，经过判断是否为目标公众号，如果是，则原样返回echaostr属性
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return echostr
     */
    @GetMapping("wx")
    @ResponseBody
    public String Wechat(String signature,String timestamp,String nonce,String echostr){
        System.out.println("signature = " + signature + ", timestamp = " + timestamp + ", nonce = " + nonce + ", echostr = " + echostr);
        if(wxService.check(signature, timestamp, nonce, echostr)){
            System.out.println("接入成功");
            return echostr;
        }else {
            System.out.println("接入失败");
        }
        return null;
    }
    @GetMapping("/index")
    public String index(String nickname, String openid, String headimgurl, Model model){
        System.out.println("nickname = " + nickname + ", openid = " + openid + ", headimgurl = " + headimgurl);
        model.addAttribute("nickname",nickname);
        model.addAttribute("openid",openid);
        model.addAttribute("headimgurl",headimgurl);
        return "userinfo";
    }
    /**
     * 为微信公众号添加按钮
     * @return 公众号添加按钮接口的返回值
     */
    @GetMapping("button")
    @ResponseBody
    public String button(){
        Button btn =new Button();
        btn.getButton().add(new ClickButton("我要注册","1"));
        btn.getButton().add(new ClickButton("完善信息","2"));
        btn.getButton().add(new ClickButton("审核查询","3"));
        JSONObject jsonObject = JSONObject.fromObject(btn);
        System.out.println(jsonObject.toString());
        String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+TokenUtil.getAccessToken().getAccessToken();
        return TokenUtil.post(url,jsonObject.toString());
    }

    /**
     * 用来接收微信服务器消息并返回消息的接口
     * @param request
     * @param response
     */
    @PostMapping("wx")
    @ResponseBody
    public void getPost(HttpServletRequest request, HttpServletResponse response){
        try{
            request.setCharacterEncoding("utf8");
            response.setCharacterEncoding("utf8");
            Map<String, String> stringStringMap = XMLDeal.parseRequst(request);
            String event = stringStringMap.get("Event");
            String openid = stringStringMap.get("FromUserName");
            logger.info("用户："+openid+" 触发了"+event+"事件");
            if(event.equals("unsubscribe")){
                logger.info("用户："+openid+"取消关注");
                return;
            }
            if(event.equals("subscribe")){
                logger.info("用户："+openid+"关注");
                return;
            }
            String key = stringStringMap.get("EventKey");
            System.out.println(stringStringMap);
            
            String url ="https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
            url = url.replace("ACCESS_TOKEN",TokenUtil.getAccessToken().getAccessToken()).replace("OPENID",openid);
            System.out.println(url);
            String userInfo = TokenUtil.get(url);
            logger.info(userInfo);
            JSONObject jsonObject = JSONObject.fromObject(userInfo);
            String nickname= (String) jsonObject.get("nickname");
            String headimgurl=jsonObject.getString("headimgurl");
            String returnURL=URL+"/index?nickname="+nickname+"&openid="+openid+"&headimgurl="+headimgurl;
            //不是按键事件，直接返回不往下执行了
            if(key==null){
                return;
            }
            switch (key){
                case "1" : returnURL="<a href=\""+URL+"/index?nickname="+ URLEncoder.encode(nickname)+"&openid="+openid+"&headimgurl="+headimgurl+"\">点击注册</a>";break;
                case "3" : returnURL="<a href=\""+URL+"/check?nickname="+ URLEncoder.encode(nickname)+"&openid="+openid+"&headimgurl="+headimgurl+"\">点击查询审核结果</a>";break;
                case "2" : returnURL="<a href=\""+URL+"/improve?nickname="+ URLEncoder.encode(nickname)+"&openid="+openid+"&headimgurl="+headimgurl+"\">点击完善信息</a>";break;
            }
            String returnXML = "<xml>\n" +
                    "  <ToUserName><![CDATA["+ stringStringMap.get("FromUserName")+ "]]></ToUserName>\n" +
                    "  <FromUserName><![CDATA["+stringStringMap.get("ToUserName")+"]]></FromUserName>\n" +
                    "  <CreateTime>"+System.currentTimeMillis()/1000+"</CreateTime>\n" +
                    "  <MsgType><![CDATA[text]]></MsgType>\n" +
                    "  <Content><![CDATA["+returnURL+"]]></Content>\n" +
                    "</xml>";
            PrintWriter writer = response.getWriter();
            writer.print(returnXML);
            writer.flush();
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @GetMapping("push")
    @ResponseBody
    public String push(){
        List<String> allOpenId = OpenIdUtil.getAllOpenId();
        return "1";
    }

    /**
     * 测试用的,不要访问
     * 该接口用来设置模板消息所属行业
     * @return
     */
//    @GetMapping("test")
//    @ResponseBody
//    public String test(){
//        String url="https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN";
//        String access_token = url.replace("ACCESS_TOKEN", TokenUtil.getAccessToken().getAccessToken());
//        System.out.println(TokenUtil.getAccessToken().getAccessToken());
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("industry_id1","1");
//        jsonObject.put("industry_id2","4");
//        return TokenUtil.post(access_token,jsonObject.toString());
////        String url="https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN";
//
//    }
    @GetMapping("sendMessage")
    @ResponseBody
    public String sendMessage(){
        String url=" https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        url=url.replace("ACCESS_TOKEN",TokenUtil.getAccessToken().getAccessToken());
        JSONObject data = new JSONObject();
        Map<Object,Object> map =new HashMap<>();
        map.put("touser","oIYCJ6e9ITFZGWkAWSXSGK82z62M");
        map.put("template_id","QcTO-HykGn6EbuR5_Dezok-dfhIfRkGOJ2dm-_M7HKI");
        map.put("appid","wx44d768b188e11568");
        map.put("data",null);
        data.putAll(map);
        return TokenUtil.post(url,data.toString());
    }



}
