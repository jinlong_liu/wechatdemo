package com.example.wechatdemo.controller;

import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.service.ChargeService;
import com.example.wechatdemo.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class ChargeController {
    @Autowired
    private RegisterService registerService;
    @Autowired
    private ChargeService chargeService;


    @GetMapping("/check")
    public String check(String nickname, String openid, String headimgurl, Model model, HttpSession session){
        System.out.println("nickname = " + nickname + ", openid = " + openid + ", headimgurl = " + headimgurl);
        SysAppuser register = registerService.isRegister(openid);
        session.setAttribute("user",register);
        model.addAttribute("nickname",nickname);
        model.addAttribute("openid",openid);
        model.addAttribute("headimgurl",headimgurl);
        return "check";
    }
    @GetMapping("/getCharge")
    @ResponseBody
    public int getCharge(HttpSession session){
        SysAppuser user = (SysAppuser) session.getAttribute("user");
        if(user==null){
            return 404;
        } else {
            return chargeService.getCharge(user);
        }
    }
}
