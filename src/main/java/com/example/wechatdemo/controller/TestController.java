package com.example.wechatdemo.controller;

import com.example.wechatdemo.utils.PushUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TestController {
    @GetMapping("/testPush")
    @ResponseBody
    public void testPush(String openID){
        List<String> openIDs = new ArrayList<>();
        openIDs.add(openID);
        PushUtil.pushMessage(openIDs);
    }
}
