package com.example.wechatdemo.timedTask;

import com.example.wechatdemo.service.PushService;
import com.example.wechatdemo.service.TrajectoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.config.ScheduledTask;

@Configuration
@EnableScheduling //开启定时任务的注解
public class ScheduleTask {

    protected static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);

    @Autowired
    private PushService pushService;

    @Autowired
    private TrajectoryService trajectoryService;


    @Scheduled(cron = "0 0 20,21,22,23 * * ? ")
    private void task(){
        logger.info("定时任务执行");
        Integer integer = pushService.pushMessage();
        logger.info("本次任务向"+integer+"人发送微信提醒");
    }
    @Scheduled(cron = "0 20,40 20,21,22,23 * * ? ")
    private void getTrajectoryTask(){
        Thread thread = new Thread(){
            public void run(){
                trajectoryService.getTrajectory();
            }
        };
        thread.start();
    }
    @Scheduled(cron = "0 0 1 * * ? ")
    private void createTable(){
        trajectoryService.createTable();
    }
}
