package com.example.wechatdemo.service.serviceImp;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.wechatdemo.bean.Trajectory;
import com.example.wechatdemo.mapper.TrajectoryMapper;
import com.example.wechatdemo.service.InsertTrajectoryService;
import org.springframework.stereotype.Service;

@Service
public class InsertTrajectoryServiceImp extends ServiceImpl<TrajectoryMapper, Trajectory> implements InsertTrajectoryService {
}
