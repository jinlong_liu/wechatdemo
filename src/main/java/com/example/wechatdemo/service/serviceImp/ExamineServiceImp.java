package com.example.wechatdemo.service.serviceImp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.wechatdemo.bean.Charge;
import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.bean.TravelInformation;
import com.example.wechatdemo.mapper.ChargeMapper;
import com.example.wechatdemo.mapper.TravelInformationMapper;
import com.example.wechatdemo.service.ExamineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ExamineServiceImp implements ExamineService {
    @Autowired
    private TravelInformationMapper travelInformationMapper;
    @Autowired
    private ChargeMapper chargeMapper;

    @Override
    public List<TravelInformation> getAllTravelInformation(SysAppuser user) {
        QueryWrapper<TravelInformation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("appuser_id",user.getAppuserId());

        return travelInformationMapper.selectList(queryWrapper);
    }

    @Override
    public TravelInformation getTravelById(String id) {
        int i = Integer.parseInt(id);
        return travelInformationMapper.selectById(id);
    }

    @Override
    public int updateTravel(Integer travelId, Date startDate, Date endDate, String startAddress, String endAddress, Integer tripMode, Integer tripObject,HttpSession session) {
//       判断该轨迹是否存在和是否已经完善
        QueryWrapper<TravelInformation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",travelId);
        TravelInformation travelInformation = travelInformationMapper.selectOne(queryWrapper);
        if(travelInformation==null){
            return 0;
        }
        if(travelInformation.getStatus()==1){
            return 2;
        }
//        判断连续两次出行的出行目的是否同为上班、回家、回程
        if(tripObject==0||tripObject==4||tripObject==5){
            QueryWrapper<TravelInformation> tripObjectWrapper = new QueryWrapper<>();
           tripObjectWrapper.eq("appuser_id",travelInformation.getAppuserId());
            List<TravelInformation> travelInformations1 = travelInformationMapper.selectList(tripObjectWrapper);
            for (TravelInformation information : travelInformations1) {
                if(information.getRanking()==travelInformation.getRanking()-1||information.getRanking()==travelInformation.getRanking()+1){
                    if(Objects.equals(information.getTripObjective(), tripObject)){
                        return 4;
                    }
                }
            }
        }

//      完善轨迹信息并存入数据库
        travelInformation.setStartDate(startDate);
        travelInformation.setEndDate(endDate);
        travelInformation.setStartAddress(startAddress);
        travelInformation.setEndAddress(endAddress);
        travelInformation.setTripMode(tripMode);
        travelInformation.setTripObjective(tripObject);
        travelInformation.setStatus(1);
        QueryWrapper<TravelInformation>  travelInformationQueryWrapper =new QueryWrapper<>();
        travelInformationQueryWrapper.eq("id",travelId);
        int update = travelInformationMapper.update(travelInformation, travelInformationQueryWrapper);
        if(update==0){
            return 0;
        }

//      判断当前用户是否所有轨迹已经完善，如果未全部完善，则返回update（该变量一定是1）
        SysAppuser user = (SysAppuser) session.getAttribute("user");
        QueryWrapper<TravelInformation> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("appuser_id",user.getAppuserId());
        List<TravelInformation> travelInformations = travelInformationMapper.selectList(queryWrapper1);
        for (TravelInformation information : travelInformations) {
            if(information.getStatus()==0){
                return update;
            }
        }
//      当前用户所有轨迹已经完善，插入审核表
        Charge charge = new Charge();
        charge.setAppuserId(user.getAppuserId());
        charge.setIsChargeStatus(2);
        int insert = chargeMapper.insert(charge);
        return update;
    }
}
