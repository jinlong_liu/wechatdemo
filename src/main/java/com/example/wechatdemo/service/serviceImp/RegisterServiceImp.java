package com.example.wechatdemo.service.serviceImp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.wechatdemo.bean.AppFamilyInformation;
import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.bean.Thirdparty;
import com.example.wechatdemo.mapper.AppFamilyInformationMapper;
import com.example.wechatdemo.mapper.SysAppuserMapper;
import com.example.wechatdemo.mapper.ThirdpartyMapper;
import com.example.wechatdemo.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class RegisterServiceImp implements RegisterService {
    @Autowired
    private AppFamilyInformationMapper appFamilyInformationMapper;
    @Autowired
    private SysAppuserMapper sysAppuserMapper;
    @Autowired
    private ThirdpartyMapper thirdpartyMapper;


    @Override
    public int register(String openid, String username, String address, String macAddress, Integer car, Integer bike, Integer sex, Integer age,
                        Integer profession, Integer isLocal, Integer income, String phone,String companyAddress) {

        QueryWrapper<SysAppuser> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("openid", openid);
        List<SysAppuser> sysAppusers = sysAppuserMapper.selectList(queryWrapper1);
        if(sysAppusers.size()>0){
            //已存在
            return 0;
        }

        QueryWrapper<Thirdparty> thirdpartyQueryWrapper = new QueryWrapper<>();
        thirdpartyQueryWrapper.eq("mac_address",macAddress);
        Integer integer = thirdpartyMapper.selectCount(thirdpartyQueryWrapper);
        if(integer>0){
            return 2;
        }




        SysAppuser sysAppuser = new SysAppuser();
        sysAppuser.setUserName(username);
        sysAppuser.setFamilyAddress(address);
        Date date = new Date();
        sysAppuser.setCreateDate(date);
        sysAppuser.setCarCount(car);
        sysAppuser.setElectricBikes(bike);
        sysAppuser.setOpenid(openid);
        sysAppuser.setFamilyMumberCount(5);
        int insert = sysAppuserMapper.insert(sysAppuser);
        QueryWrapper<SysAppuser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid",openid);
        SysAppuser returnAppuser = sysAppuserMapper.selectOne(queryWrapper);

        Thirdparty thirdparty = new Thirdparty();
        thirdparty.setAppuserId(returnAppuser.getAppuserId());
        thirdparty.setMacAddress(macAddress);

        int insert1 = thirdpartyMapper.insert(thirdparty);
        if(insert1==0){
            return 0;
        }

        AppFamilyInformation appFamilyInformation = new AppFamilyInformation();
        appFamilyInformation.setAge(age);
        appFamilyInformation.setAppuserId(returnAppuser.getAppuserId());
        appFamilyInformation.setIncome(income);
        appFamilyInformation.setIsPermanentAddress(isLocal);
        appFamilyInformation.setProfession(profession);
        appFamilyInformation.setSex(sex);
        appFamilyInformation.setCompanyAddress(companyAddress);
        return appFamilyInformationMapper.insert(appFamilyInformation);
    }

    @Override
    public SysAppuser isRegister(String openid) {
        QueryWrapper<SysAppuser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid", openid);
        return sysAppuserMapper.selectOne(queryWrapper);
    }
}
