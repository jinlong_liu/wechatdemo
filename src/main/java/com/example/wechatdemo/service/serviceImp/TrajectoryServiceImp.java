package com.example.wechatdemo.service.serviceImp;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.wechatdemo.bean.Charge;
import com.example.wechatdemo.bean.Thirdparty;
import com.example.wechatdemo.bean.Trajectory;
import com.example.wechatdemo.mapper.ChargeMapper;
import com.example.wechatdemo.mapper.ThirdpartyMapper;
import com.example.wechatdemo.mapper.TrajectoryMapper;
import com.example.wechatdemo.mapper.TravelInformationMapper;
import com.example.wechatdemo.service.InsertTrajectoryService;
import com.example.wechatdemo.service.TrajectoryService;
import com.example.wechatdemo.utils.DateUtils;
import com.example.wechatdemo.utils.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.ScheduledTask;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class TrajectoryServiceImp implements TrajectoryService {
    @Autowired
    private TrajectoryMapper trajectoryMapper;
    @Autowired
    private ThirdpartyMapper thirdpartyMapper;
    @Autowired
    private TravelInformationMapper travelInformationMapper;
    @Autowired
    private InsertTrajectoryService insertTrajectoryService;

    @Autowired                                  //me
    private ChargeMapper chargeMapper;           //me

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:dd");

    protected static final Logger logger = LoggerFactory.getLogger(TrajectoryServiceImp.class);

    @Override
    public void getTrajectory() {
        QueryWrapper<Thirdparty> queryWrapper = new QueryWrapper<>();
        List<Thirdparty> thirdparties = thirdpartyMapper.selectList(queryWrapper);
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH)+1;
        int day = now.get(Calendar.DAY_OF_MONTH);
        String dataBaseName;
        if(day>=10){
            dataBaseName = "trajectory_"+year+"_"+month+"_"+day;
        }else {
            dataBaseName = "trajectory_"+year+"_"+month+"_0"+day;
        }

//        //获取以完善但审核没通过的id
//        QueryWrapper<Charge> chargeQueryWrapper = new QueryWrapper<Charge>();  //me
//        List<Charge> charges = chargeMapper.selectList(chargeQueryWrapper);   //me
//        List<Integer> unchargefectUserId = new ArrayList<>();                //me
//        for (Charge charge : charges){                                       //me
//            if (charge.getIsChargeStatus()==2) { //me
//                unchargefectUserId.add(charge.getAppuserId());                  //me
//            }                                                                   //me
//        }
//        //获取已完善的id
//        QueryWrapper<TravelInformation> travelInformationQueryWrapper = new QueryWrapper<>();
//        travelInformationQueryWrapper.eq("status",1);
//        List<TravelInformation> travelInformations = travelInformationMapper.selectList(travelInformationQueryWrapper);
//        List<Integer> perfectUserId = new ArrayList<>();
//        for (TravelInformation travelInformation : travelInformations) {
//            perfectUserId.add(travelInformation.getAppuserId());
//        }
//        List<Thirdparty> thirdparties1 = new ArrayList<>();
//        for(int i = 0;i<thirdparties.size();i++){
//            if(!perfectUserId.contains(thirdparties.get(i).getAppuserId())){
//                thirdparties1.add(thirdparties.get(i));
//            }
//            if(unchargefectUserId.contains(thirdparties.get(i).getAppuserId())){
//                thirdparties1.add(thirdparties.get(i));
//            }
//        }

        //获取以完善审核通过或未审核的id
        QueryWrapper<Charge> chargeQueryWrapper = new QueryWrapper<Charge>();  //me
        List<Charge> charges = chargeMapper.selectList(chargeQueryWrapper);   //me
        List<Integer> unchargefectUserId = new ArrayList<>();                //me
        for (Charge charge : charges) {                                       //me
            if (charge.getIsChargeStatus() == 1||charge.getIsChargeStatus() == 2) { //me
                unchargefectUserId.add(charge.getAppuserId());                  //me
            }                                                                   //me
        }
        //获取mac为null或者为“”的userid数组
        QueryWrapper<Thirdparty> thirdpartyQueryWrapper = new QueryWrapper<>();
        thirdpartyQueryWrapper.eq("mac_address", "").or().isNull("mac_address");
        List<Thirdparty> thirdpartiesNull = thirdpartyMapper.selectList(thirdpartyQueryWrapper);
        List<Integer> thirdpartiesNullId = new ArrayList<>();
        for (Thirdparty tp : thirdpartiesNull) {
            thirdpartiesNullId.add(tp.getAppuserId());
        }

        //做减法
        List<Thirdparty> thirdparties1 = new ArrayList<>();
        for (int i = 0; i < thirdparties.size(); i++) {
            if (!unchargefectUserId.contains(thirdparties.get(i).getAppuserId())&&!thirdpartiesNullId.contains(thirdparties.get(i).getAppuserId())) {
                thirdparties1.add(thirdparties.get(i));
            }
        }
        logger.info("本次获取数据的人数："+thirdparties1.size());
        logger.info("需获取user的第三方信息："+thirdparties1);
        for (Thirdparty thirdparty : thirdparties1) {
            try {
                List<Trajectory> trajectories = HttpUtil.getGpsByIMEI(thirdparty.getMacAddress(), thirdparty.getAppuserId());
                if(trajectories!=null){
                    trajectoryMapper.deleteByDayAndUserId(dataBaseName,thirdparty.getAppuserId());
//                    trajectoryMapper.insertTrajectoryList(dataBaseName,trajectories);
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("insert into ").append(dataBaseName).append(" (appuser_id,start_date,end_date,trajectory_gps,address) values");
                    for (Trajectory trajectory : trajectories) {
                       // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:dd");
                        //System.err.println("for循环中string前的obj："+trajectory.toString());
                        System.err.println("for循环中string前的obj-smformat："+DateUtils.stringToDate(trajectory.getStartDate()));
 //                       String string = "("+trajectory.getAppuserId()+",'"+simpleDateFormat.format(trajectory.getStartDate())+"','"+simpleDateFormat.format(trajectory.getEndDate())+"','"+trajectory.getTrajectoryGps()+"','"+trajectory.getAddress()+"'),";
//                        trajectoryMapper.insertTrajectory(dataBaseName,trajectory.getAppuserId(),
//                                trajectory.getStartDate(),trajectory.getEndDate(),trajectory.getTrajectoryGps(),trajectory.getAddress());
                        //System.err.println("string前的的打印块："+formatter.format(DateUtils.date2LocalDateTime(trajectory.getStartDate())));
                        String string = "("+trajectory.getAppuserId()+",'"+DateUtils.stringToDate(trajectory.getStartDate()) + "','"+ DateUtils.stringToDate(trajectory.getEndDate())+"','"+trajectory.getTrajectoryGps()+"','"+trajectory.getAddress()+"'),";
                        System.err.println("real插入块："+string);
                        stringBuilder.append(string);
                    }
                    StringBuilder stringBuilder1 = stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                    stringBuilder1.append(";");
                    System.out.println("stringBuilder1 = " + stringBuilder1.toString());
                    trajectoryMapper.insertTrajectoryList(stringBuilder1.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void createTable() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH)+1;
        int day = now.get(Calendar.DAY_OF_MONTH);
        String dataBaseName;
        if(day>=10){
            dataBaseName = "trajectory_"+year+"_"+month+"_"+day;
        }else {
            dataBaseName = "trajectory_"+year+"_"+month+"_0"+day;
        }
        trajectoryMapper.createTable(dataBaseName);
    }
}
