package com.example.wechatdemo.service.serviceImp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.bean.TravelInformation;
import com.example.wechatdemo.mapper.SysAppuserMapper;
import com.example.wechatdemo.mapper.TravelInformationMapper;
import com.example.wechatdemo.service.PushService;
import com.example.wechatdemo.utils.OpenIdUtil;
import com.example.wechatdemo.utils.PushUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.ScheduledTask;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PushServiceImp implements PushService {

    protected static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);

    @Autowired
    private SysAppuserMapper userMapper;

    @Autowired
    private TravelInformationMapper travelInformationMapper;


    @Override
    public Integer pushMessage() {
        QueryWrapper<TravelInformation> queryWrapper = new QueryWrapper<>();
        List<TravelInformation> travelInformations = travelInformationMapper.selectList(queryWrapper);
        List<Integer> appUserIds = new ArrayList<>();
        for (TravelInformation travelInformation : travelInformations) {
            if(travelInformation.getStatus()==0){
                appUserIds.add(travelInformation.getAppuserId());
            }
        }
        if(appUserIds.size()==0){
            logger.info("本次无人未完善轨迹信息");
            return 0;
        }
        QueryWrapper<SysAppuser> sysAppuserQueryWrapper = new QueryWrapper<>();
        for (Integer appUserId : appUserIds) {
            sysAppuserQueryWrapper.or().eq("appuser_id",appUserId);
        }
        List<SysAppuser> sysAppusers = userMapper.selectList(sysAppuserQueryWrapper);

        List<String> ImperfectUserOpenid = new ArrayList<>();
        for (SysAppuser sysAppuser : sysAppusers) {
            ImperfectUserOpenid.add(sysAppuser.getOpenid());
        }
        PushUtil.pushMessage(ImperfectUserOpenid);
        logger.info("向"+sysAppusers.toString()+"发送了提醒");
        return sysAppusers.size();
    }
}
