package com.example.wechatdemo.service.serviceImp;

import com.example.wechatdemo.service.WxService;
import com.example.wechatdemo.utils.TokenUtil;
import com.example.wechatdemo.utils.XMLDeal;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;

@Service
public class WxServiceImp implements WxService {
    private static final String TOKEN = "kinrontest";

    @Override
    public boolean check(String signature, String timestamp, String nonce, String echostr) {
        String[] strs = new String[]{TOKEN,timestamp,nonce};
        Arrays.sort(strs);

        String str = strs[0]+strs[1]+strs[2];
        String mysig = encryption(str);
        System.out.println(mysig);
        System.out.println(signature);
        return mysig.equals(signature);
    }

    /**
     * 进行sha1加密
     * @param str
     * @return
     */
    @Override
    public String encryption(String str) {
        try {
            //获取一个加密对象
            MessageDigest md = MessageDigest.getInstance("sha1");
            //获取加密数组
            byte[] digest = md.digest(str.getBytes());
            char[] chars={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
            StringBuilder stringBuilder = new StringBuilder();
            for(byte b:digest){
                stringBuilder.append(chars[(b>>4)&15]);
                stringBuilder.append(chars[b&15]);
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getUserInfo(String openid) {
        String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
        url = url.replace("ACCESS_TOKEN", TokenUtil.getAccessToken().getAccessToken()).replace("OPENID",openid);
        TokenUtil.get(url);
        return null;
    }

    @Override
    public void returnRegister(HttpServletRequest request, HttpServletResponse response) {
        try{
            request.setCharacterEncoding("utf8");
            response.setCharacterEncoding("utf8");
            Map<String, String> stringStringMap = XMLDeal.parseRequst(request);
            String url ="https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
            url = url.replace("ACCESS_TOKEN",TokenUtil.getAccessToken().getAccessToken()).replace("OPENID",stringStringMap.get("FromUserName"));
            String userInfo = TokenUtil.get(url);
            System.out.println(userInfo);
            JSONObject jsonObject = JSONObject.fromObject(userInfo);
            String nickname= (String) jsonObject.get("nickname");
            String openid=jsonObject.getString("openid");
            String headimgurl=jsonObject.getString("headimgurl");
            String returnURL="http://kinrontest.free.idcfengye.com/index?nickname="+nickname+"&openid="+openid+"&headimgurl="+headimgurl;
            String returnXML = "<xml>\n" +
                    "  <ToUserName><![CDATA["+ stringStringMap.get("FromUserName")+ "]]></ToUserName>\n" +
                    "  <FromUserName><![CDATA["+stringStringMap.get("ToUserName")+"]]></FromUserName>\n" +
                    "  <CreateTime>"+System.currentTimeMillis()/1000+"</CreateTime>\n" +
                    "  <MsgType><![CDATA[text]]></MsgType>\n" +
                    "  <Content><![CDATA[注册地址:"+returnURL+"]]></Content>\n" +
                    "</xml>";
            PrintWriter writer = response.getWriter();
            writer.print(returnXML);
            writer.flush();
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
