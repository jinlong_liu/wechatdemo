package com.example.wechatdemo.service.serviceImp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.wechatdemo.bean.Charge;
import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.mapper.ChargeMapper;
import com.example.wechatdemo.service.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChargeServiceImp implements ChargeService {

    @Autowired
    private ChargeMapper chargeMapper;

    @Override
    public int getCharge(SysAppuser user) {
        QueryWrapper<Charge> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("appuser_id",user.getAppuserId());
        //Charge charge = chargeMapper.selectOne(queryWrapper);
        List<Charge> chargeList = chargeMapper.selectList(queryWrapper);

        if(chargeList==null){
            return 405;
        }
        //選取max-id 為最近的審核
        Charge chargeOne = chargeList.get(0);
        for (Charge charge : chargeList) {
            if (charge.getId()>chargeOne.getId()){
                chargeOne = charge;
            }
        }
        return chargeOne.getIsChargeStatus();
    }
}
