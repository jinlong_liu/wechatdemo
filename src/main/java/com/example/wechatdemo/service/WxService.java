package com.example.wechatdemo.service;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public interface WxService {
    public boolean check(String signature,String timestamp,String nonce,String echostr);

    public String encryption(String str);

    public String getUserInfo(String openid);

    public void returnRegister(HttpServletRequest request, HttpServletResponse response);
}
