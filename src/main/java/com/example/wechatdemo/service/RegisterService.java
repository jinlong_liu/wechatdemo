package com.example.wechatdemo.service;

import com.example.wechatdemo.bean.SysAppuser;
import org.springframework.stereotype.Service;

@Service
public interface RegisterService {
    public int register(String openid,String username,String address,String macAddress,Integer car,Integer bike
            ,Integer sex,Integer age,Integer profession,Integer isLocal,Integer income,String phone,String companyAddress);

    /**
     * 判断该openid是否注册账号
     * @param openid
     * @return 0表示用户已经注册或注册失败 1表示注册成功 2表示手环MAC已被使用
     */
    public SysAppuser isRegister(String openid);
}
