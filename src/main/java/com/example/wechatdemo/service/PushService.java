package com.example.wechatdemo.service;

public interface PushService {
    /**
     * 给没有当天没有完善信息的用户推送提醒消息
     * @return 推送的人数
     */
    public Integer pushMessage();
}
