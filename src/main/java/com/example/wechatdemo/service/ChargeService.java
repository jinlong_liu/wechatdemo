package com.example.wechatdemo.service;

import com.example.wechatdemo.bean.SysAppuser;
import org.springframework.stereotype.Service;

@Service
public interface ChargeService {
    /**
     * 获取审核结果
     * @param user
     * @return 1为审核成功 0未审核失败 2未审核 405为没有审核记录
     */
    public int getCharge(SysAppuser user);
}
