package com.example.wechatdemo.service;

import org.springframework.stereotype.Service;

@Service
public interface TrajectoryService {
    public void getTrajectory();
    public void createTable();
}
