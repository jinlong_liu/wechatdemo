package com.example.wechatdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wechatdemo.bean.Trajectory;
import org.springframework.stereotype.Service;

@Service
public interface InsertTrajectoryService extends IService<Trajectory> {
}
