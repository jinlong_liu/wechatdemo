package com.example.wechatdemo.service;

import com.example.wechatdemo.bean.SysAppuser;
import com.example.wechatdemo.bean.TravelInformation;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Service
public interface ExamineService {
    /**
     * 获取用户所有轨迹
     * @param openid
     * @return
     */
    List<TravelInformation> getAllTravelInformation(SysAppuser user);

    TravelInformation getTravelById(String id);

    /**
     * 完善轨迹信息
     * @param travelId
     * @param startDate
     * @param endDate
     * @param startAddress
     * @param endAddress
     * @param tripMode
     * @param tripObject
     * @return 1表示更新成功 2表示已完善 0表示失败 4表示连续两次出行目的相同
     */
    int updateTravel(Integer travelId, Date startDate,
                     Date endDate, String startAddress, String endAddress, Integer tripMode, Integer tripObject, HttpSession session);
}
