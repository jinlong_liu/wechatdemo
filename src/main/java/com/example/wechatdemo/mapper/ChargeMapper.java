package com.example.wechatdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wechatdemo.bean.Charge;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ChargeMapper extends BaseMapper<Charge> {
}
