package com.example.wechatdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wechatdemo.bean.Trajectory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
@Mapper
public interface TrajectoryMapper extends BaseMapper<Trajectory> {
    /**
     * 获取到信息前执行删除操作
     * @param dataBaseName
     * @param appUserId
     * @return
     */
    @Delete("delete from ${dataBaseName} where appuser_id = #{appUserId}")
    public Integer deleteByDayAndUserId(String dataBaseName,Integer appUserId);

    /**
     * 创建表
     * @param tableName
     */
    @Update("CREATE TABLE `${tableName}` (`id` int(11) NOT NULL AUTO_INCREMENT,`appuser_id` int(11) DEFAULT NULL,`start_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,`end_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,`trajectory_gps` varchar(255) DEFAULT NULL,`address` varchar(255) DEFAULT NULL,PRIMARY KEY (`id`) USING BTREE) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;")
    public void createTable(String tableName);

    /**
     * 插入数据
     * @param tableName
     * @param appUserId
     * @param startDate
     * @param endDate
     * @param trajectoryGps
     * @param address
     */
    @Insert("insert into ${tableName} (appuser_id,start_date,end_date,trajectory_gps,address) values(#{appUserId},#{startDate},#{endDate},#{trajectoryGps},#{address})")
    public void insertTrajectory(String tableName, Integer appUserId, Date startDate, Date endDate, String trajectoryGps, String address);
    @Select("${sql}")
    public void insertTrajectoryList(String sql);


}
