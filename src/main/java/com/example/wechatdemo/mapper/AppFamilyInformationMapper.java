package com.example.wechatdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wechatdemo.bean.AppFamilyInformation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AppFamilyInformationMapper extends BaseMapper<AppFamilyInformation> {
}
