package com.example.wechatdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wechatdemo.bean.Thirdparty;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ThirdpartyMapper extends BaseMapper<Thirdparty> {
    /**
     * 获取需要获取GPS点的第三方信息
     * @return
     */
    @Select("SELECT * from sys_appuser_thirdparty where mac_address is not null and mac_address !='' and appuser_id not in(SELECT appuser_id from charge where is_charge_status = 1 or is_charge_status = 2)")
    public List<Thirdparty> getThirdParty();
}
